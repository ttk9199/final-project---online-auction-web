//const passport=require('passport');
const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const accountM = require('../models/accountModel');
const funcs = require('../utils/extensionFunc');
const run = funcs.errorHandle;
const hash = require('../utils/hash');

module.exports = {
    local: passport => {
        passport.serializeUser(function (user, done) {
            done(null, user.f_ID);
        });

        passport.deserializeUser(async (id, done) => {
            const [user, err] = await run(accountM.getById(id));
            done(err, user);
        });

        passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',

        },
            async (email, password, done) => {
                const [user, err] = await run(accountM.getByEmail(email));
                if (err) {
                    return done(err);
                }
                if (user === null) {
                    return done(null, false, { message: 'Incorrect Email' });
                }
                if (!hash.comparePassword(password, user.f_Password)) {
                    return done(null, false, { message: 'Incorrect password' });
                }
                return done(null, user);
            }

        ));
    },

    facebook: passport=>{

    },

    app: app => {
        app.use((req, res, next) => {
            res.locals.user = req.user;
            next();
        })

    }


};




