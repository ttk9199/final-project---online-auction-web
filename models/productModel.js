const db = require('../utils/db');
const tbnNameProduct = 'products';

exports.productsHome1 = async (page = 0) => {
    /*const sql = `SELECT ProID,ProName,TinyDes, Price FROM ${tbnNameProduct}
                limit ${(page - 1) * 9},9;`;*/
    const sql = `select* from ql_banhang.products ORDER BY Price desc limit ${(page-1)*4},4;`;
    const row = await db.load(sql);
    return row;
};
exports.productsHome2 = async (page = 0) => {
    /*const sql = `SELECT ProID,ProName,TinyDes, Price FROM ${tbnNameProduct}
                limit ${(page - 1) * 9},9;`;*/
    const sql = `select* from ql_banhang.products ORDER BY Quantity desc limit ${(page-1)*4},4;`;
    const row = await db.load(sql);
    return row;
};
exports.productsHomePage = async (page = 0) => {
    /*const sql = `SELECT ProID,ProName,TinyDes, Price FROM ${tbnNameProduct}
                limit ${(page - 1) * 9},9;`;*/
    const sql = `select* from ql_banhang.products ORDER BY Quantity desc limit ${(page-1)*4},4;`;
    const row = await db.load(sql);
    return row;
};
exports.productsID = async (id, page = 1) => {
    const sql = `SELECT ProID,ProName,TinyDes FROM ${tbnNameProduct}
                where CatID=${id}
                limit ${(page - 1) * 9},9;`;
    const row = await db.load(sql);
    return row;
};
exports.detailProID = async (id) => {
    const sql = `SELECT ProName,FullDes,Price,Quantity FROM ${tbnNameProduct}
                    where ProID=${id};`;
    const row = await db.load(sql);
    return row;
};

