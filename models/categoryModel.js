const db = require('../utils/db');
const funcs = require('../utils/extensionFunc');
const run = funcs.errorHandle;
const tbnNameCategory = 'Categories';
const tbnNameProduct = 'products';

module.exports = {
    all: async () => {
        const sql = `SELECT * FROM ${tbnNameCategory}`;
        const [rows, err] = await run(db.load(sql));
        if (err) {
            throw err;
        }
        return rows;
    },
    del: async id => {
        const [nr, err] = await run(db.del(tbnNameCategory, idField, id));
        if (err) {
            throw err;
        }
        return nr;
    },
    update: async entity => {
        const [nr, err] = await run(db.del(tbnNameCategory, idField, entity));
        if (err) {
            throw err;
        }
        return nr;
    },
    add: async entity => {
        const [id, err] = await run(db.del(tbnNameCategory, entity));
        if (err) {
            throw err;
        }
        return id;
    }
}
