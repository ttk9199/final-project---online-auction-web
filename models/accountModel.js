const db=require('../utils/db');
const funcs=require('../utils/extensionFunc');
const run=funcs.errorHandle;
const tbName='users';

module.exports={
    add: async  user =>{
        const [id,err] = await  run(db.add(tbName,user));
        if(err){
            throw err;
        }
        console.log(id);
       // const id=await db.add(tbName,user);
        return id;
    },
    getByEmail: async email =>{
        let sql=`SELECT* FROM  ${tbName}  WHERE f_Email = '${email}';`;
        /*const params=[tbName,'f_Username',username];
        sql=db.mysql.format(sql,params);*/
        console.log("accountM/getByUsername: sql= ",sql);
        const [rs,err]=await run(db.load(sql));
        if(err){
            throw err;
        }
        else{
            if(rs.length>0){
                return rs[0];
            }
            return null;
        }
    },
    getById: async id =>{
        let sql=`SELECT* FROM ${tbName} WHERE f_ID='${id}';`;
        const rt=await db.load(sql);
        if(rt.length>0){
            return rt[0];
        }
        return null;
    }
};
