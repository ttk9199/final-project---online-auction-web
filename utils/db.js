var mysql = require('mysql');

var connection =()=> {
  return  mysql.createConnection({
    host: 'localhost',
    user: 'toan',
    password: 'nguyentro1309#1',
    database: 'ql_banhang'
  });
}

exports.load = (sql) => {
  return new Promise((resole, reject) => {
    const con = connection();
    con.connect((err) => {
      if (err) {
        reject(err);
      }
    });
    con.query(sql, (err, result, fields) => {
      if (err) {
        reject(err);
      }
      resole(result);
    });
    con.end();
  });
};

exports.add=(tbName,entity)=>{
  return new Promise((resole,reject)=>{
    const con=connection();
    con.connect((err)=>{
      if(err){
        reject(err);
      }
    });
    const sql=`INSERT INTO ${tbName} SET ?`;
    con.query(sql,entity,(error,result,fields)=>{
      if(error){
        reject(error);
      }
      //resole(result.insertId);
      //console.log(result.insertId);
    });
    con.end();
  });
};

exports.del=(tbName,idField,id)=>{
  return new Promise((resole,reject)=>{
    const con=connection();
    con.connect(err=>{
      if(err){
        reject(err);
      }
    });
    let sql='DELETE FROM ?? WHERE ??=?';
    const params=[tbName,idField,id];
    sql=mysql.format(sql,params);
    con.query(sql,(error,results,fields)=>{
      if(error){
        reject(error);
      }
      else{
        resole(results.affectedRows);
      }
    });
    con.end();
  });
};

exports.update=(tbName,idField,entity)=>{
  return new Promise((resole,reject)=>{
    const con=connection();
    con.connect(err=>{
      if(err){
        reject(err);
      }
    });
    const id=entity[idField];
    delete entity[idField];
    let sql=`UPDATE ${tbName} SET ? WHERE ${idField}=${id};`;
    con.query(sql,entity,(error,results,fields)=>{
      if(error){
        reject(error);
      }
      else{
        resole(results.changedRows);
      }
    });
    con.end();
  });
};


