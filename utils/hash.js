/*var validator=require('express-validator');
var passport=require('passport');
var LocalStrategy=require('passport-local').Strategy;


passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  passport.use('local.register',new LocalStrategy({
    usernameField: 'email', // Tên input mail
    passwordField: 'password', //intput pass
    passReqToCallback: true
  }, function(req,email,password,done){
    //validator các input từ trang đăng kí
    

  }));*/

  const sha=require('sha.js');
  const hashLength=64;
  function hashWithSalt(str,salt){
      const preHash=str+salt;
      const hash=sha('sha256').update(preHash).digest('hex');
      const pwHash=hash+salt;
      return pwHash;
  }

  module.exports={
    getHashWithSalt: str=>{
      const salt=Date.now().toString(16);
      const pwHash=hashWithSalt(str,salt);
      return pwHash;
    },

    comparePassword: (pwIn,pwSaved)=>{
      const salt=pwSaved.substring(hashLength,pwSaved.length);
      const pwHash=hashWithSalt(pwIn,salt);
      return pwHash===pwSaved;
  
    },
  };