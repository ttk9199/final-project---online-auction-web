var express = require('express');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var exphbsSections = require('express-handlebars-sections');
var path = require('path');
var request=require('request');

var ssesion= require('express-session');
var passport=require('passport');
var flash=require('connect-flash');
var validator=require('express-validator');
//utils

var config=require('./middleware/config');
//controller
var detailProModel=require('./controllers/detailCtrL')
var categories = require('./controllers/categoryCtrL');
//model
var categoryModel = require('./models/categoryModel.js');
var productModel = require('./models/productModel');
var Handlebars = require('handlebars');
var app = express();
const hbs=exphbs.create({
  defaultLayout: 'main',
  extname: 'hbs',
});
exphbsSections(hbs);
app.engine('hbs',hbs.engine);
app.set('view engine', 'hbs');

require('./middleware/config').local(passport);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(ssesion({
  secret: 'whRAA968dAWPkIsU14UjojvtRNPbk6hK',
  resave: false,
  saveUninitialized: true
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

require('./middleware/config').app(app);

//require('./controllers/accountCtrL')(app,passport);
//app.use(passport.initialize());
//app.use(passport.session());



app.use('/public', express.static(path.join(__dirname, 'public')));

app.set('views', __dirname + '/views');
//contrller của trang chi tiết
app.use('/product',detailProModel);
// Controller của trang category
app.use('/cat', categories);
//
// controller của trang home
app.get('/', async (req, res) => {
  const menuside = await categoryModel.all();
  const contentSide = await productModel.productsHome1(1);

 
  const contentSide2 = await productModel.productsHome2(1);

  const contentSidePage = await productModel.productsHomePage(1);
  

  const loopFor = { from: 1, page: 1, link: "/home" };// dữ liệu phân trang
  res.render('category', {
    dataMenu: menuside,
    dataContenSide: contentSide,
    pagination: loopFor,
    dataContenSide2: contentSide2,
    dataContenSidePage: contentSidePage
  })

});
app.get('/home/page=:page', async (req, res) => {
  // xử lý phân trang
  const page = parseInt(req.params.page);

  const loopFor = {page: page, link: "/home" };// dữ liệu phân trang
  const menuside = await categoryModel.all();            // dữ liệu category
  const contentSide = await productModel.productsHome1(page);  // dữ liệu product
  const contentSide2 = await productModel.productsHome2(page);
  const contentSidePage = await productModel.productsHomePage(page);

  console.log(loopFor);
  // 

  res.render('category', {
    dataMenu: menuside,
    dataContenSide: contentSide,
    pagination: loopFor,
    dataContenSide2: contentSide2,
    dataContenSidePage: contentSidePage
  })
});

app.use('/account',require('./controllers/accountCtrL')(passport));

app.post('/account/login',(req,res)=>{
  if(req.body.captcha===undefined||req.body.captcha===''||req.body.captcha===null){
    return res.json({"success": false,"msg":"Please select captcha"});
  }

  // Secret key
  const secretKey='6LeUeMYUAAAAAJdWQm9L1QBoCVdZhixWtVtN0csW';

  //Verify URL
  const verifyUrl=`https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;

  //Make request to verifyUrl
  request(verifyUrl,(err,response, body)=>{
    body=JSON.parse(body);
  });

  //If not successful
  if(body.success!==undefined&&!body.success){
    return res.json({"success": false,"msg":"Failed captcha verification"});
  }

  //If successful
  return res.json({"success": true,"msg":"Captcha passed"});
});


// tạo vòng lặp for
Handlebars.registerHelper('for', function (from, to, block) {
  var accum = '';
  for (var i = from; i < to; i++)
    accum += block.fn(i);
  return accum;
});
Handlebars.registerHelper('incPage', (value, option) => {
  return parseInt(value) + 1;
});
Handlebars.registerHelper('decrPage', (value, option) => {
  return parseInt(value) - 1 > 0 ?  parseInt(value) - 1:1;
});
Handlebars.registerHelper('MakePagin',(page,link ,option)=>{
  let prev;
  if (page % 3 === 0) {
    prev = page - 2;
  } else {
    prev = parseInt(page / 3) * 3 + 1;
  }
  var output='';
  let a;
  for(let i=prev;i<prev+3;i++){
    output+=`<li class="page-item ${i===page?'active':''}"><a class="page-link" href="${link}/page=${i} ">${i}</a></li>`;
    a=1;
  }
  console.log(a);
  return output;
})


//error
require('./middleware/errors')(app);
// khởi tạo server
var server = app.listen(3000, () => {
  var port = server.address().port;

  console.log(`Example app listening at ${port}`);
})

