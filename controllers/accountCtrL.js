const express = require('express');
const router = express.Router();
const sha = require('sha.js');
const accountM = require('../models/accountModel');
//const passport=require('../middleware/config');
const passport=require('passport');
const funcs = require('../utils/extensionFunc');
const run = funcs.errorHandle;
const hash=require('../utils/hash');
const nodemailer=require('nodemailer');
//const jwt=require('jsonwebtoken');

const hashLength = 64;

module.exports=function(passport){
    router.get('/login', (req, res) => {
        res.render('./account/login', {
            layout: 'account',
        });
    });
    
    router.get('/register', (req, res) => {
        res.render('./account/register', {
            layout: 'account',
        });
    });
    
    router.get('/profile', (req, res) => {
        if (!req.user) {
            res.redirect('/account/login');
        }
        else {
            res.render('./account/profile', {
                layout: 'account',
            });
        }
    });
    
    router.get('/logout',(req,res)=>{
        if(!req.user){
            res.redirect('/account/login');
        }
        else{
            req.logout();
            res.redirect('/');
        }
    });
    
    router.post('/login', async (req, res,next) => {
        passport.authenticate('local',(err,user,info)=>{
            console.log('accountCtrL/post/login: ',{
                err,
                user,
                info,
            });
            if(err){
                return next(err);
            }
            if(!user){
                return res.render('account/login',{
                    layout: 'account',
                    message: info.message,
    
                });
            }
            req.login(user,err=>{
                if(err){
                    return next(err);
                }
                return res.redirect('/');
            });
    
        })(req,res,next);
        
    });
    /*router.post('/login', async (req, res) => {
        const email = req.body.email;
        const password = req.body.password;
        const user = await accountM.getByEmail(email);
        if (user === null) {
            res.redirect('/');
            console.log('tài khoản chưa tồn tại');
            return;
        }
    
        const pwDb = user.f_Password;
        const salt = pwDb.substring(hashLength, pwDb.length);
        const preHash = password + salt;
        const hash = sha('sha256').update(preHash).digest('hex');
        const pwHash = hash + salt;
        if (pwHash === pwDb) {
            req.session.user = user.f_ID;
            res.redirect('/');
        }
    });
    
    router.post('/createAccount', async (req, res) => {
        const email = req.body.email;
        const password = req.body.password;
        const address=req.body.address;
        const name=req.body.name;
        const temp=await accountM.getByEmail(email);
    
        if(temp){
           res.send('Tài khoản đã có');
           return;
        }
    
        if(password.length<8){
            res.send('Mật khẩu phải có ít nhất 8 kí tự');
            return;
        }
    
        //Thực hiện các thao tác xử lý password bản rõ
        const salt = Date.now().toString(16);
        const preHash = password + salt;
        const hash = sha('sha256').update(preHash).digest('hex');
        const pwHash = hash + salt;
        const user = {
            f_Address: address,
            f_Password: pwHash,
            f_Name: name,//req.body.name,
            f_Email: email,//req.body.email,
            f_DOB: new Date,//req.body.dob,
            f_Permission: 0,       // Phân quyền user, admistrator
        };
        const uId = await accountM.add(user);
        if (uId > 0) {
            req.session.user = uId;
            res.redirect('/');
        }
        res.redirect('/');
    });*/

 

    
 

    router.post('/register', async (req, res,next) => {
        const email = req.body.email;
        const password = req.body.password;
        const address=req.body.address;
        const name=req.body.name;
        const temp=await accountM.getByEmail(email);
    
        if(temp){
           res.send('Tài khoản đã có');
           return;
        }
    
        if(password.length<8){
            res.send('Mật khẩu phải có ít nhất 8 kí tự');
            return;
        }
    
        //Thực hiện các thao tác xử lý password bản rõ
        const salt = Date.now().toString(16);
        //const preHash = password + salt;
        //const hash = sha('sha256').update(preHash).digest('hex');
       // const pwHash = hash + salt;
       const pwHash=hash.getHashWithSalt(password);
        const user = {
            f_Address: address,
            f_Password: pwHash,
            f_Name: name,//req.body.name,
            f_Email: email,//req.body.email,
            f_DOB: new Date,//req.body.dob,
            f_Permission: 0,       // Phân quyền user, admistrator
        };
        console.log('1:' );
        const [uId,err] =  await run(accountM.add(user));
        //const uId=await accountM.add(user);
        console.log(uId);
        if(err){
            next(err);
            console.log(err);
            return;
        }
        if (uId > 0) {
            //res.redirect('/');
            //req.session.user = uId;
           req.login(user,err=>{
               if(err){
                   return next(err);
               }
               return res.redirect('/');
           });
        }
       
    });  
    
    return router;
}
    


//module.exports = router;