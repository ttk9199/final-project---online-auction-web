var express = require('express');
var categoryModel = require('../models/categoryModel.js');
var productModel = require('../models/productModel.js');
var router = express.Router();

module.exports=function(passport){
    router.get('/upload',(req,res,next)=>{
        res.render('./upload/upload',{
            layout: 'upload'
        })
    })
    return router
}