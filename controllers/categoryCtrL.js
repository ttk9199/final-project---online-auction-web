var express = require('express');
var categoryModel = require('../models/categoryModel.js');
var productModel = require('../models/productModel.js');
var router = express.Router();

router.get('/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    let loopFor = {page: 1,link:'/cat/'+id};
    const menuside = await categoryModel.all().catch((err) => console.log('caught it'));;
    for (const item of menuside) {
        if (item.CatID === id) {
            item.isActive = true;
        } else item.isActive = false;
    }
    const productsID = await productModel.productsID(id).catch((err) => console.log('caught it'));
    res.render('category', {
        dataMenu: menuside,
        dataContenSide: productsID,
        pagination: loopFor
    });
});
router.get('/:id/page=:page', async (req, res) => {
    const id = parseInt(req.params.id);
    const page = parseInt(req.params.page);
    console.log(page);
    const loopFor = {page: page, link: '/cat/' + id };// dữ liệu phân trang
    const menuside = await categoryModel.all().catch((err) => console.log('caught it'));;
    for (const item of menuside) {
        if (item.CatID === id) {
            item.isActive = true;
        } else item.isActive = false;
    }
    const productsID = await productModel.productsID(id, page).catch((err) => console.log('caught it'));;
    res.render('category', {
        dataMenu: menuside,
        dataContenSide: productsID,
        pagination: loopFor
    });
});

router.post('/edit', async (req, res, next) => {
    const id = parseInt(req.body.CatID);
    const [cats, err] = await run(mCat.all());
    if (err) {
        return next(err);
    }
    for (const cat of cats) {
        cat.isEdit = false;
        if (cat.CatID === id) {
            cat.isEdit = true;
        }
    }
    res.render('./admin/category',{
        layout: 'admin',
        pageTitle: 'Categories',
        cats
    });
});

router.post('/del',async(req,res,next)=>{
    const id = parseInt(req.body.CatID);
    const [nr, err] = await run(mCat.del(id));
    if(err){
        return next(err);
    }
    res.render('./admin/cat');
});

module.exports = router;